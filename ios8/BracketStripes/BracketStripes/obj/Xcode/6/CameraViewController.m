// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "CameraViewController.h"

@implementation CameraViewController

@synthesize bracketChangeDidChange = _bracketChangeDidChange;
@synthesize bracketModeControl = _bracketModeControl;
@synthesize cameraPreviewView = _cameraPreviewView;
@synthesize cameraShutterButton = _cameraShutterButton;

- (IBAction)CameraShutterDidPress:(UIButton *)sender {
}

- (IBAction)bracketChangeDidChange:(id)sender {
}

@end
