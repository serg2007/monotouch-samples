using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

namespace KeychainTouchID
{
	public class Test
	{
		public string Name { get; set; }

		public string Details { get; set; }

		public Action Method { get; set; }
	}

}
